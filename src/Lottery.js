import React, { Component } from 'react';
import Number from './Number.js';
import './Lottery.css';

const classColors = {
    lotto: "pink",
    lottery: "blue",
    mini: "green",
    default: 'grey'
}

class Lottery extends Component {

    static defaultProps = {
        title: "Lotto",
        numBalls: 6,
        maxNum: 40
    }


    constructor(props){

        super(props);
        this.state = {
            numbers: []
        };
    }

    random = () => {
        return Math.floor(Math.random() * this.props.maxNum + 1);
    }


    generate = () => {

        this.setState({ numbers: [] });
        const newNumbers = [];
        while(newNumbers.length < this.props.numBalls){
            const randNum = this.random();
            if(newNumbers.includes(randNum) === false){
                newNumbers.push(randNum);
            }
        }

        this.setState({numbers: newNumbers});

    }

    pickColor = () => {
        switch(this.props.title){
            case 'Lotto': return classColors.lotto;
            case 'Lotto Special': return classColors.lottery;
            case 'Mini Daily': return classColors.mini;
            default: return classColors.default;
        }
    }

    render() {

        let color = this.pickColor();


        return(
            <div>
                <h2>{this.props.title}</h2>
                <div className="Lottery-Numbers">
                    {this.state.numbers.map(number => <Number key={number} number={number} color={color}/>)}
                </div>
                <button className="btn" onClick={this.generate}>Generate Numbers</button>
            </div>
        );
    }
}

export default Lottery;