import React from 'react';
import Lottery from './Lottery';
import './App.css';

function App() {
  return (
    <div className="App">
      <h1>My Lottery</h1>
      <Lottery title="Mini Daily" numBalls={4} maxNum={10} /> 
      <Lottery title="Lotto Special" numBalls={7} maxNum={40} />
      <Lottery />
    </div>
  );
}

export default App;
